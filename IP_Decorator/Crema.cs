﻿namespace IP_Decorator
{
    class Crema : AgregadoDecorator
    {
        public Crema(BebidaComponent bebida) : base(bebida) { }
        public override double Costo => _bebida.Costo + 10;
        public override string Descripcion => $"{_bebida.Descripcion}, Crema";
    }
}
