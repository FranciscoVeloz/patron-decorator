﻿namespace IP_Decorator
{
    class Azucar : AgregadoDecorator
    {
        public Azucar(BebidaComponent bebida) : base(bebida) { }
        public override double Costo => _bebida.Costo + 5;
        public override string Descripcion => $"{_bebida.Descripcion}, Azucar";
    }
}
