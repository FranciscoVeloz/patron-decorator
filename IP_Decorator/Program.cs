﻿using System;

namespace IP_Decorator
{
    class Program
    {
        static void Main(string[] args)
        {
            BebidaComponent cafe = new CafeSolo();
            cafe = new Leche(cafe);
            cafe = new Azucar(cafe);

            Console.WriteLine($"Producto: {cafe.Descripcion} -- Precio: ${cafe.Costo}");

            Console.ReadKey();
        }
    }
}
