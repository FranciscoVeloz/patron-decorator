﻿namespace IP_Decorator
{
    class CafeDescaifenado : BebidaComponent
    {
        public override double Costo => 15;
        public override string Descripcion => "Café descafeinado";
    }
}
